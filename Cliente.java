public class Cliente{
	
	String nome;
	String numeroDaConta;
	String rg;
	String cpf;
	double salario;
public void setNome (String umNome){
	this.nome = umNome;
}

public void setNumeroDaConta (String umNumeroDaConta){
	this.numeroDaConta = umNumeroDaConta;
}	

public void setRg (String umRg){
	this.rg = umRg;
}

public void setCpf (String umCpf){
	this.cpf = umCpf;
}
public String getNome(){
	return nome;
}

public String getNumeroDaConta(){
	return numeroDaConta;
}

public String getRg(){
	return rg;
}
public String getCpf(){
	return cpf;
}

public double getSalario(){
	return salario;
}
public void setSalario (double umSalario){
	this.salario = salario;
}

}
